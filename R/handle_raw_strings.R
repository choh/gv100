#' Split raw GV100 data into fields.
#'
#' This is primarily intended as helper function to the read functions,
#' but can be called on their own.
#' They take strings and split them into the respective
#' fields as per the GV100AD/NAD format definition.
#'
#' @param raw_data A vector (atomic or list) containing the raw strings to
#' process.
#' @param raw_names If true field names as given in the format specification
#' will be used, if false more readable names will be used for the columns.
#' @param drop_empty If true all empty columns will be dropped, if false
#' they will be kept.
#'
#' The format definitions are (c) by the Statistisches Bundesamt,
#' Wiesbaden/Germany. Reproduction is explicitly allowd in the format
#' definition given attribution.
#'
#' @return The data in a ldata.frame for GV100AD, in a list of data.frames
#' for GV100NAD.
#' @name handle_raw_strings
NULL