# gv100 - An R package to read files from Destatis

This package contains tools to read the file format used by Destatis to 
store information about various administrative levels, including population, 
coordinates, post codes, etc. 

While the format is not complex, it is nonetheless a bit tedious to read, 
thus I started creating a package for it.
